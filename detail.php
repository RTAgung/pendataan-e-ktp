<?php
include "database.php";
include "utils.php";
include "session_manager.php";
include "security.php";

if (!$sm->checkLogin()) header("Location: login.php");

if (isset($_GET['id']))
  $id = $_GET['id'];
else
  header("Location: index.php");

$data = $db->readById($id);

if ($data == null) header("Location: index.php");

foreach ($data as $key => $value) {
  if ($key != "Id")
    $data[$key] = decryptSuper($value);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Eh... Katepe</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>

  <head>
    <nav class="navbar navbar-dark bg-primary">
      <div class="container-fluid">
        <a class="navbar-brand" href="index.php">Eh... Katepe</a>
        <div class="d-flex">
          <a class="btn btn-outline-light mx-2" href="pin.php">Kunci</a>
          <a class="btn btn-outline-light mx-2" href="process.php?f=logout">Keluar</a>
        </div>
      </div>
    </nav>
  </head>

  <main class="container my-5">
    <div class="border border-primary p-5">
      <div class="p-5" style="align-content: center;">
        <img src="<?= $data['Foto'] ?>" width="225px" height="300px" style="object-fit: cover;" class="mx-auto d-block" alt="Foto">
        <table class="table mx-auto my-5" style="width: 60%;">
          <tr>
            <td style="padding: 20px; font-weight: bold;">Nomor Induk</td>
            <td style="padding: 20px; font-weight: bold;"><?= $data['NIK'] ?></td>
          </tr>
          <tr>
            <td style="padding: 20px;">Nama</td>
            <td style="padding: 20px;"><?= $data['Nama'] ?></td>
          </tr>
          <tr>
            <td style="padding: 20px;">Tempat/Tgl Lahir</td>
            <td style="padding: 20px;"><?= formatTTL($data['TempatLahir'], $data['TanggalLahir']) ?></td>
          </tr>
          <tr>
            <td style="padding: 20px;">Jenis Kelamin</td>
            <td style="padding: 20px;"><?= $data['JenisKelamin'] ?></td>
          </tr>
          <tr>
            <td style="padding: 20px;">Gol. Darah</td>
            <td style="padding: 20px;"><?= $data['GolDarah'] ?></td>
          </tr>
          <tr>
            <td style="padding: 20px;">Alamat</td>
            <td style="padding: 20px;"><?= $data['Alamat'] ?></td>
          </tr>
          <tr>
            <td style="padding: 20px 60px;">RT/RW</td>
            <td style="padding: 20px;"><?= formatRtRw($data['RT'], $data['RW']) ?></td>
          </tr>
          <tr>
            <td style="padding: 20px 60px;">Kel/Desa</td>
            <td style="padding: 20px;"><?= $data["KelDesa"] ?></td>
          </tr>
          <tr>
            <td style="padding: 20px 60px;">Kecamatan</td>
            <td style="padding: 20px;"><?= $data['Kecamatan'] ?></td>
          </tr>
          <tr>
            <td style="padding: 20px;">Agama</td>
            <td style="padding: 20px;"><?= $data['Agama'] ?></td>
          </tr>
          <tr>
            <td style="padding: 20px;">Status Perkawinan</td>
            <td style="padding: 20px;"><?= $data['StatusPerkawinan'] ?></td>
          </tr>
          <tr>
            <td style="padding: 20px;">Pekerjaan</td>
            <td style="padding: 20px;"><?= $data['Pekerjaan'] ?></td>
          </tr>
          <tr>
            <td style="padding: 20px;">Kewarganegaraan</td>
            <td style="padding: 20px;"><?= $data['Kewarganegaraan'] ?></td>
          </tr>
          <tr>
            <td style="padding: 20px;">Berlaku Hingga</td>
            <td style="padding: 20px;"><?= formatBerlakuHingga($data['MasaBerlaku']) ?></td>
          </tr>
        </table>
        <div class="d-grid gap-2 col-3 mx-auto">
          <a href="edit.php?id=<?= $data['Id'] ?>" class="btn btn-primary">Edit</a>
          <a href="delete.php?id=<?= $data['Id'] ?>" class="btn btn-danger">Hapus</a>
        </div>
      </div>
    </div>
  </main>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>

</html>