<?php
include "session_manager.php";

if (!$sm->checkLogin()) header("Location: login.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Eh... Katepe</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>

  <head>
    <nav class="navbar navbar-dark bg-primary">
      <div class="container-fluid">
        <a class="navbar-brand" href="index.php">Eh... Katepe</a>
        <div class="d-flex">
          <a class="btn btn-outline-light mx-2" href="process.php?f=logout">Keluar</a>
        </div>
      </div>
    </nav>
  </head>

  <main class="container my-5">
    <div class="border border-primary p-5">
      <form action="process.php?f=pin" method="POST">
        <legend>Verifikasi Pin</legend>
        <div class="mb-3">
          <label for="inputPin" class="form-label">Pin</label>
          <input type="number" class="form-control" id="inputPin" placeholder="pin" name="pin" required>
        </div>
        <div class="mb-3">
          <input type="submit" class="form-control btn btn-primary" value="Verifikasi" name="submit">
        </div>
      </form>
    </div>
  </main>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>

</html>