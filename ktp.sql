-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2021 at 04:13 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ktp`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `Id` int(11) NOT NULL,
  `Username` varchar(200) NOT NULL,
  `Email` varchar(200) NOT NULL,
  `Password` varchar(200) NOT NULL,
  `Pin` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`Id`, `Username`, `Email`, `Password`, `Pin`) VALUES
(6, 'YnnAb`Hu`', 'pple.ygekaky9ymye1gkrs@j`', '3fc0a7acf087f549ac2b266baf94b8b1', '81dc9bdb52d04dc20036dbd8313ed055');

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `Id` int(11) NOT NULL,
  `NIK` varchar(200) NOT NULL,
  `Nama` varchar(200) NOT NULL,
  `TempatLahir` varchar(200) NOT NULL,
  `TanggalLahir` varchar(200) NOT NULL,
  `JenisKelamin` varchar(200) NOT NULL,
  `GolDarah` varchar(200) NOT NULL,
  `Alamat` varchar(200) NOT NULL,
  `RT` varchar(200) NOT NULL,
  `RW` varchar(200) NOT NULL,
  `KelDesa` varchar(200) NOT NULL,
  `Kecamatan` varchar(200) NOT NULL,
  `Agama` varchar(200) NOT NULL,
  `StatusPerkawinan` varchar(200) NOT NULL,
  `Pekerjaan` varchar(200) NOT NULL,
  `Kewarganegaraan` varchar(200) NOT NULL,
  `MasaBerlaku` varchar(200) NOT NULL,
  `Foto` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`Id`, `NIK`, `Nama`, `TempatLahir`, `TanggalLahir`, `JenisKelamin`, `GolDarah`, `Alamat`, `RT`, `RW`, `KelDesa`, `Kecamatan`, `Agama`, `StatusPerkawinan`, `Pekerjaan`, `Kewarganegaraan`, `MasaBerlaku`, `Foto`) VALUES
(21, '1593260437154826', 'F  BOHOUAFU`OWI`', 'MER`FER`VTT`TY``', '2-0`006`01``0-``', 'URJJ-TTUR', '-', 'YB PAJGN APJPPW`', '0`7`', '-', 'YMT`M M`DDK`SMG`', 'IIXI RFRXFYK QFRDPX`HDBK`', 'NF`XR`Q``', 'MXH`P T`WVY`FL``', 'WHYLQ`SH`', 'ZLQ`', 'EGU`QDP`G G`YTB`', 'p173n`h284g`o395``t406``o51.``/62p``'),
(22, '0624951384627351', 'OUWAU  OIHF`BFO`', 'YNJJVWUKP', '2-0`007`02``0-``', 'YNDNVJAYW', 'C', 'YB PAJGN APJPPW`', '0`2`', '0`4`', 'YMT`M M`DDK`SMG`', 'IIXI RFRXFYK QFRDPX`HDBK`', 'NF`XR`Q``', 'PN`FS`B``', 'FJJRDQABJ', 'ZLQ`', 'EGU`QDP`G G`YTB`', 'p044n`h933g`o822``t711``o66.``/55p``');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Username` (`Username`);

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `NIK` (`NIK`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `data`
--
ALTER TABLE `data`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
