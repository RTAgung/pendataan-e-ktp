<?php
include "session_manager.php";

if (!$sm->checkLogin()) header("Location: login.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Eh... Katepe</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>

  <head>
    <nav class="navbar navbar-dark bg-primary">
      <div class="container-fluid">
        <a class="navbar-brand" href="index.php">Eh... Katepe</a>
        <div class="d-flex">
          <a class="btn btn-outline-light mx-2" href="pin.php">Kunci</a>
          <a class="btn btn-outline-light mx-2" href="process.php?f=logout">Keluar</a>
        </div>
      </div>
    </nav>
  </head>

  <main class="container my-5">
    <div class="border border-primary p-5">
      <form action="process.php?f=create" method="POST" enctype="multipart/form-data">
        <legend>Tambah Data</legend>
        <div class="mb-3">
          <label for="inputNik" class="form-label">Nik</label>
          <input type="text" class="form-control" id="inputNik" placeholder="nik" name="nik" required>
        </div>
        <div class="mb-3">
          <label for="inputNama" class="form-label">Nama</label>
          <input type="text" class="form-control" id="inputNama" placeholder="nama" name="nama" required>
        </div>
        <div class="mb-3">
          <label for="inputTempatLahir" class="form-label">Tempat Lahir</label>
          <input type="text" class="form-control" id="inputTempatLahir" placeholder="tempat lahir" name="tempatLahir" required>
        </div>
        <div class="mb-3">
          <label for="inputTanggalLahir" class="form-label">Tanggal Lahir</label>
          <input type="date" class="form-control" id="inputTanggalLahir" placeholder="tanggal lahir" name="tanggalLahir" required>
        </div>
        <div class="mb-3">
          <label for="inputJenisKelamin" class="form-label">Jenis Kelamin</label>
          <select class="form-select" id="inputJenisKelamin" name="jenisKelamin" required>
            <option value="Laki-laki" selected>Laki-laki</option>
            <option value="Perempuan">Perempuan</option>
          </select>
        </div>
        <div class="mb-3">
          <label for="inputGolDarah" class="form-label">Golongan Darah</label>
          <select class="form-select" id="inputGolDarah" name="golDarah" required>
            <option value="-" selected>-</option>
            <option value="A">A</option>
            <option value="B">B</option>
            <option value="AB">AB</option>
            <option value="O">O</option>
          </select>
        </div>
        <div class="mb-3">
          <label for="inputAlamat" class="form-label">Alamat</label>
          <input type="text" class="form-control" id="inputAlamat" placeholder="alamat" name="alamat" required>
        </div>
        <div class="mb-3">
          <div class="row g-3">
            <div class="col">
              <label for="inputRT" class="form-label">RT</label>
              <input type="number" class="form-control" id="inputRT" placeholder="RT" name="rt">
            </div>
            <div class="col">
              <label for="inputRW" class="form-label">RW</label>
              <input type="number" class="form-control" id="inputRW" placeholder="RW" name="rw">
            </div>
          </div>
        </div>
        <div class="mb-3">
          <div class="row g-3">
            <div class="col">
              <label for="inputKelDesa" class="form-label">Kel/Desa</label>
              <input type="text" class="form-control" id="inputKelDesa" placeholder="kel/desa" name="kelDesa">
            </div>
            <div class="col">
              <label for="inputKecamatan" class="form-label">Kecamatan</label>
              <input type="text" class="form-control" id="inputKecamatan" placeholder="kecamatan" name="kecamatan">
            </div>
          </div>
        </div>
        <div class="mb-3">
          <label for="inputAgama" class="form-label">Agama</label>
          <select class="form-select" id="inputAgama" name="agama" required>
            <option value="Islam" selected>Islam</option>
            <option value="Kristen">Kristen</option>
            <option value="Hindu">Hindu</option>
            <option value="Budha">Budha</option>
          </select>
        </div>
        <div class="mb-3">
          <label for="inputStatus" class="form-label">Status Perkawinan</label>
          <select class="form-select" id="inputStatus" name="status" required>
            <option value="Belum Kawin" selected>Belum Kawin</option>
            <option value="Kawin">Kawin</option>
          </select>
        </div>
        <div class="mb-3">
          <label for="inputPekerjaan" class="form-label">Pekerjaan</label>
          <input type="text" class="form-control" id="inputPekerjaan" placeholder="pekerjaan" name="pekerjaan" required>
        </div>
        <div class="mb-3">
          <label for="inputKewarganegaraan" class="form-label">Kewarganegaraan</label>
          <input type="text" class="form-control" id="inputKewarganegaraan" placeholder="kewarganegaraan" name="kewarganegaraan" required>
        </div>
        <div class="mb-3">
          <label for="inputMasaBerlaku" class="form-label">Masa Berlaku</label>
          <div class="row g-3">
            <div class="col-10">
              <input type="date" class="form-control" id="inputMasaBerlaku" placeholder="masa berlaku" name="masaBerlaku" value="1000-01-01" required>
            </div>
            <div class="col-2">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="Seumur Hidup" id="flexCheckSeumurHidup" name="masaBerlakuSeumurHidup">
                <label class="form-check-label" for="flexCheckSeumurHidup">Seumur Hidup</label>
              </div>
            </div>
          </div>
        </div>
        <div class="mb-3">
          <label for="formFileFoto" class="form-label">Foto</label>
          <input class="form-control" type="file" id="formFileFoto" name="foto" accept="image/png, image/jpeg" required>
        </div>
        <div class="mb-3">
          <input type="submit" class="form-control btn btn-primary" value="Tambah Data" name="submit">
        </div>
      </form>
    </div>
  </main>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>

</html>