<?php
include "database.php";
include "utils.php";
include "session_manager.php";
include "security.php";

if (!$sm->checkLogin()) header("Location: login.php");

if (isset($_GET['id']))
  $id = $_GET['id'];
else
  header("Location: index.php");

$data = $db->readById($id);

if ($data == null) header("Location: index.php");

foreach ($data as $key => $value) {
  if ($key != "Id" && $key != "NIK")
    $data[$key] = decryptSuper($value);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Eh... Katepe</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>

  <head>
    <nav class="navbar navbar-dark bg-primary">
      <div class="container-fluid">
        <a class="navbar-brand" href="index.php">Eh... Katepe</a>
        <div class="d-flex">
          <a class="btn btn-outline-light mx-2" href="pin.php">Kunci</a>
          <a class="btn btn-outline-light mx-2" href="process.php?f=logout">Keluar</a>
        </div>
      </div>
    </nav>
  </head>

  <main class="container my-5">
    <div class="border border-primary p-5">
      <form action="process.php?f=edit" method="POST" enctype="multipart/form-data">
        <legend>Edit Data</legend>
        <input type="hidden" name="id" value="<?= $data['Id'] ?>">
        <input type="hidden" name="nik" value="<?= $data['NIK'] ?>">
        <div class="mb-3">
          <label for="inputNik" class="form-label">Nik</label>
          <input type="text" class="form-control" id="inputNik" placeholder="nik" value="<?= $data['NIK'] ?>" required disabled>
        </div>
        <div class="mb-3">
          <label for="inputNama" class="form-label">Nama</label>
          <input type="text" class="form-control" id="inputNama" placeholder="nama" name="nama" value="<?= $data['Nama'] ?>" required>
        </div>
        <div class="mb-3">
          <label for="inputTempatLahir" class="form-label">Tempat Lahir</label>
          <input type="text" class="form-control" id="inputTempatLahir" placeholder="tempat lahir" name="tempatLahir" value="<?= $data['TempatLahir'] ?>" required>
        </div>
        <div class="mb-3">
          <label for="inputTanggalLahir" class="form-label">Tanggal Lahir</label>
          <input type="date" class="form-control" id="inputTanggalLahir" placeholder="tanggal lahir" name="tanggalLahir" value="<?= $data['TanggalLahir'] ?>" required>
        </div>
        <div class="mb-3">
          <label for="inputJenisKelamin" class="form-label">Jenis Kelamin</label>
          <select class="form-select" id="inputJenisKelamin" name="jenisKelamin" required>
            <?php
            $selectedLK = "";
            $selectedPR = "";
            if ($data['JenisKelamin'] == "LAKI-LAKI") {
              $selectedLK = "selected";
            } else if ($data['JenisKelamin'] == "PEREMPUAN") {
              $selectedPR = "selected";
            }
            ?>
            <option value="Laki-laki" <?= $selectedLK ?>>Laki-laki</option>
            <option value="Perempuan" <?= $selectedPR ?>>Perempuan</option>
          </select>
        </div>
        <div class="mb-3">
          <label for="inputGolDarah" class="form-label">Golongan Darah</label>
          <select class="form-select" id="inputGolDarah" name="golDarah" required>
            <?php
            $selectedGD = array("", "", "", "", "");;
            if ($data['GolDarah'] == "-") {
              $selectedGD[0] = "selected";
            } else if ($data['GolDarah'] == "A") {
              $selectedGD[1] = "selected";
            } else if ($data['GolDarah'] == "B") {
              $selectedGD[2] = "selected";
            } else if ($data['GolDarah'] == "AB") {
              $selectedGD[3] = "selected";
            } else if ($data['GolDarah'] == "O") {
              $selectedGD[4] = "selected";
            }
            ?>
            <option value="-" <?= $selectedGD[0] ?>>-</option>
            <option value="A" <?= $selectedGD[1] ?>>A</option>
            <option value="B" <?= $selectedGD[2] ?>>B</option>
            <option value="AB" <?= $selectedGD[3] ?>>AB</option>
            <option value="O" <?= $selectedGD[4] ?>>O</option>
          </select>
        </div>
        <div class="mb-3">
          <label for="inputAlamat" class="form-label">Alamat</label>
          <input type="text" class="form-control" id="inputAlamat" placeholder="alamat" name="alamat" value="<?= $data['Alamat'] ?>" required>
        </div>
        <div class="mb-3">
          <div class="row g-3">
            <div class="col">
              <label for="inputRT" class="form-label">RT</label>
              <input type="number" class="form-control" id="inputRT" placeholder="RT" name="rt" value="<?= $data['RT'] ?>">
            </div>
            <div class="col">
              <label for="inputRW" class="form-label">RW</label>
              <input type="number" class="form-control" id="inputRW" placeholder="RW" name="rw" value="<?= $data['RW'] ?>">
            </div>
          </div>
        </div>
        <div class="mb-3">
          <div class="row g-3">
            <div class="col">
              <label for="inputKelDesa" class="form-label">Kel/Desa</label>
              <input type="text" class="form-control" id="inputKelDesa" placeholder="kel/desa" name="kelDesa" value="<?= $data['KelDesa'] ?>">
            </div>
            <div class="col">
              <label for="inputKecamatan" class="form-label">Kecamatan</label>
              <input type="text" class="form-control" id="inputKecamatan" placeholder="kecamatan" name="kecamatan" value="<?= $data['Kecamatan'] ?>">
            </div>
          </div>
        </div>
        <div class="mb-3">
          <label for="inputAgama" class="form-label">Agama</label>
          <select class="form-select" id="inputAgama" name="agama" required>
            <?php
            $selectedA = array("", "", "", "");;
            if ($data['Agama'] == "ISLAM") {
              $selectedA[0] = "selected";
            } else if ($data['Agama'] == "KRISTEN") {
              $selectedA[1] = "selected";
            } else if ($data['Agama'] == "HINDU") {
              $selectedA[2] = "selected";
            } else if ($data['Agama'] == "BUDHA") {
              $selectedA[3] = "selected";
            }
            ?>
            <option value="Islam" <?= $selectedA[0] ?>>Islam</option>
            <option value="Kristen" <?= $selectedA[1] ?>>Kristen</option>
            <option value="Hindu" <?= $selectedA[2] ?>>Hindu</option>
            <option value="Budha" <?= $selectedA[3] ?>>Budha</option>
          </select>
        </div>
        <div class="mb-3">
          <label for="inputStatus" class="form-label">Status Perkawinan</label>
          <select class="form-select" id="inputStatus" name="status" required>
            <?php
            $selectedSP = array("", "");;
            if ($data['StatusPerkawinan'] == "BELUM KAWIN") {
              $selectedSP[0] = "selected";
            } else if ($data['StatusPerkawinan'] == "KAWIN") {
              $selectedSP[1] = "selected";
            }
            ?>
            <option value="Belum Kawin" <?= $selectedSP[0] ?>>Belum Kawin</option>
            <option value="Kawin" <?= $selectedSP[1] ?>>Kawin</option>
          </select>
        </div>
        <div class="mb-3">
          <label for="inputPekerjaan" class="form-label">Pekerjaan</label>
          <input type="text" class="form-control" id="inputPekerjaan" placeholder="pekerjaan" name="pekerjaan" value="<?= $data['Pekerjaan'] ?>" required>
        </div>
        <div class="mb-3">
          <label for="inputKewarganegaraan" class="form-label">Kewarganegaraan</label>
          <input type="text" class="form-control" id="inputKewarganegaraan" placeholder="kewarganegaraan" name="kewarganegaraan" value="<?= $data['Kewarganegaraan'] ?>" required>
        </div>
        <div class="mb-3">
          <label for="inputMasaBerlaku" class="form-label">Masa Berlaku</label>
          <div class="row g-3">
            <?php
            $checkedSH = "";
            $date = $data['MasaBerlaku'];
            if ($data['MasaBerlaku'] == "SEUMUR HIDUP") {
              $checkedSH = "checked";
              $date = date("Y-m-d", strtotime("1000-01-01"));
            }
            ?>
            <div class="col-10">
              <input type="date" class="form-control" id="inputMasaBerlaku" placeholder="masa berlaku" name="masaBerlaku" value="<?= $date ?>" required>
            </div>
            <div class="col-2">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="Seumur Hidup" id="flexCheckSeumurHidup" name="masaBerlakuSeumurHidup" <?= $checkedSH ?>>
                <label class="form-check-label" for="flexCheckSeumurHidup">Seumur Hidup</label>
              </div>
            </div>
          </div>
        </div>
        <div class="mb-3">
          <label for="formFileFoto" class="form-label">Foto</label>
          <input class="form-control" type="file" id="formFileFoto" name="foto" accept="image/png, image/jpeg">
        </div>
        <div class="mb-3">
          <input type="submit" class="form-control btn btn-primary" value="Edit Data" name="submit">
        </div>
      </form>
    </div>
  </main>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>

</html>