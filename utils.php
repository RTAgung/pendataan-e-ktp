<?php

function formatTanggal($tanggal)
{
  $hasil = date('d-m-Y', strtotime($tanggal));
  return $hasil;
}

function formatTTL($tempatLahir, $tanggalLahir)
{
  $hasil = "$tempatLahir, " . formatTanggal($tanggalLahir);
  return $hasil;
}

function formatRtRw($rt, $rw)
{
  $hasil = "$rt/$rw";
  return $hasil;
}

function formatBerlakuHingga($data)
{
  $hasil = "";
  if ($data == "SEUMUR HIDUP")
    $hasil = $data;
  else
    $hasil = formatTanggal($data);
  return $hasil;
}
