<?php
$db = new Database();

class Database
{
  var $HOSTNAME = "localhost";
  var $USERNAME = "root";
  var $PASSWORD = "";
  var $DATABASE = "ktp";

  var $connection;

  function __construct()
  {
    $this->connection = mysqli_connect($this->HOSTNAME, $this->USERNAME, $this->PASSWORD, $this->DATABASE);
  }

  function read()
  {
    $query = "SELECT * FROM `data`";
    $data = mysqli_query($this->connection, $query);
    $hasil = null;
    while ($row = mysqli_fetch_array($data)) {
      $hasil[] = $row;
    }
    return $hasil;
  }

  function create(
    $nik,
    $nama,
    $tempatLahir,
    $tanggalLahir,
    $jenisKelamin,
    $golDarah,
    $alamat,
    $RT,
    $RW,
    $kelDesa,
    $kecamatan,
    $agama,
    $statusPerkawinan,
    $pekerjaan,
    $kewarganegaraan,
    $masaBerlaku,
    $foto
  ) {
    $query = "INSERT INTO `data` VALUES('', '$nik', '$nama', '$tempatLahir','$tanggalLahir','$jenisKelamin','$golDarah','$alamat','$RT','$RW','$kelDesa','$kecamatan','$agama','$statusPerkawinan','$pekerjaan','$kewarganegaraan','$masaBerlaku','$foto')";
    mysqli_query($this->connection, $query);
  }

  function readById($id)
  {
    $query = "SELECT * FROM `data` WHERE Id = '$id'";
    $data = mysqli_query($this->connection, $query);
    $hasil[0] = null;
    while ($row = mysqli_fetch_array($data)) {
      $hasil[0] = $row;
    }
    return $hasil[0];
  }

  function update(
    $id,
    $nama,
    $tempatLahir,
    $tanggalLahir,
    $jenisKelamin,
    $golDarah,
    $alamat,
    $RT,
    $RW,
    $kelDesa,
    $kecamatan,
    $agama,
    $statusPerkawinan,
    $pekerjaan,
    $kewarganegaraan,
    $masaBerlaku,
    $foto
  ) {
    if ($foto == "") {
      $query = "UPDATE `data` SET Nama = '$nama', TempatLahir = '$tempatLahir', TanggalLahir = '$tanggalLahir', JenisKelamin = '$jenisKelamin', GolDarah = '$golDarah', Alamat = '$alamat', RT = '$RT', RW = '$RW', KelDesa = '$kelDesa', Kecamatan = '$kecamatan', Agama = '$agama', StatusPerkawinan = '$statusPerkawinan', Pekerjaan = '$pekerjaan', Kewarganegaraan = '$kewarganegaraan', MasaBerlaku = '$masaBerlaku' WHERE Id = '$id'";
    } else {
      $query = "UPDATE `data` SET Nama = '$nama', TempatLahir = '$tempatLahir', TanggalLahir = '$tanggalLahir', JenisKelamin = '$jenisKelamin', GolDarah = '$golDarah', Alamat = '$alamat', RT = '$RT', RW = '$RW', KelDesa = '$kelDesa', Kecamatan = '$kecamatan', Agama = '$agama', StatusPerkawinan = '$statusPerkawinan', Pekerjaan = '$pekerjaan', Kewarganegaraan = '$kewarganegaraan', MasaBerlaku = '$masaBerlaku', Foto = '$foto' WHERE Id = '$id'";
    }
    mysqli_query($this->connection, $query);
  }

  function delete($id)
  {
    $query = "DELETE FROM `data` WHERE Id = '$id'";
    mysqli_query($this->connection, $query);
  }

  function checkExistAccount($username)
  {
    $query = "SELECT * FROM `accounts` WHERE Username = '$username'";
    $data = mysqli_query($this->connection, $query);
    $hasil = mysqli_num_rows($data);
    return $hasil;
  }

  function createAccount($username, $email, $password, $pin)
  {
    $query = "INSERT INTO `accounts` VALUES('', '$username', '$email', '$password', '$pin')";
    mysqli_query($this->connection, $query);
  }

  function checkLogin($username, $password)
  {
    $query = "SELECT * FROM `accounts` WHERE Username = '$username' AND `Password` = '$password'";
    $data = mysqli_query($this->connection, $query);
    $hasil = mysqli_num_rows($data);
    return $hasil;
  }

  function checkPin($username, $pin)
  {
    $query = "SELECT * FROM `accounts` WHERE Username = '$username' AND Pin = '$pin'";
    $data = mysqli_query($this->connection, $query);
    $hasil = mysqli_num_rows($data);
    return $hasil;
  }
}
