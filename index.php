<?php
include "database.php";
include "utils.php";
include "session_manager.php";
include "security.php";

if (!$sm->checkLogin()) header("Location: login.php");

$data = $db->read();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Eh... Katepe</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
  <style>
    .hover:hover {
      background-color: rgba(179, 198, 255, 0.5);
    }
  </style>
</head>

<body>

  <head>
    <nav class="navbar navbar-dark bg-primary">
      <div class="container-fluid">
        <a class="navbar-brand" href="index.php">Eh... Katepe</a>
        <div class="d-flex">
          <a class="btn btn-outline-light mx-2" href="pin.php">Kunci</a>
          <a class="btn btn-outline-light mx-2" href="process.php?f=logout">Keluar</a>
        </div>
      </div>
    </nav>
  </head>

  <main class="container my-5">
    <div class="border border-primary p-5">
      <a href="create.php" class="btn btn-primary mb-4">Tambah Data</a>
      <div>
        <div class="row row-cols-1 row-cols-md-2 g-4">
          <?php
          if ($data != null) {
            foreach ($data as $item) {
              foreach ($item as $key => $value) {
                if ($key != "Id")
                  $item[$key] = decryptSuper($value);
              }
          ?>
              <div class="col">
                <a href="detail.php?id=<?= $item['Id'] ?>" style="text-decoration: none; color: black">
                  <div class="card hover">
                    <div class="row g-0">
                      <div class="col-md-4">
                        <img src="<?= $item['Foto'] ?>" height="170px" width="127px" style="object-fit: cover;" class="mx-auto my-auto d-block" alt="Foto">
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title"><?= $item['NIK'] ?></h5>
                          <p class="card-text"><?= $item['Nama'] ?></p>
                          <p class="card-text"><?= formatTTL($item['TempatLahir'], $item['TanggalLahir']) ?></p>
                          <p class="card-text"><?= $item['JenisKelamin'] ?></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
          <?php
            }
          }
          ?>
        </div>
      </div>
    </div>
  </main>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>

</html>