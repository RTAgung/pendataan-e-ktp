<?php
if (isset($_GET['f'])) {
  include 'database.php';
  include 'session_manager.php';
  include 'security.php';

  if ($_GET["f"] == "signup") {
    if (isset($_POST['submit'])) {
      foreach ($_POST as $key => $value) {
        ${$key} = $value;
      }

      $username = encryptSuper($username);
      $email = encryptSuper($email);
      if ($db->checkExistAccount($username) < 1) {
        $password = md5($password);
        $pin = md5($pin);
        $db->createAccount($username, $email, $password, $pin);
        header("Location: login.php");
      } else {
        header("Location: signup.php");
      }
    }
  }

  if ($_GET['f'] == "login") {
    if (isset($_POST['submit'])) {
      foreach ($_POST as $key => $value) {
        ${$key} = $value;
      }

      $password = md5($password);
      $username2 = encryptSuper($username);
      if ($db->checkLogin($username2, $password) == 1) {
        $sm->setLogin($username);
        header("Location: index.php");
      } else {
        header("Location: login.php");
      }
    }
  }

  if ($_GET["f"] == "create") {
    if (isset($_POST['submit'])) {
      foreach ($_POST as $key => $value) {
        if ($key != "nik")
          ${$key} = encryptSuper(strtoupper($value));
        else
          ${$key} = strtoupper($value);
      }

      $target_dir = "photo/";
      $target_file = $target_dir . $nik . ".png";

      if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) {
        $foto = encryptSuper($target_file);
        $nik = encryptSuper($nik);

        if (isset($masaBerlakuSeumurHidup))
          $masaBerlaku = $masaBerlakuSeumurHidup;
        if ($rt == "")
          $rt = "-";
        if ($rw == "")
          $rw = "-";
        if ($kelDesa == "")
          $kelDesa = "-";
        if ($kecamatan == "")
          $kecamatan = "-";

        $db->create(
          $nik,
          $nama,
          $tempatLahir,
          $tanggalLahir,
          $jenisKelamin,
          $golDarah,
          $alamat,
          $rt,
          $rw,
          $kelDesa,
          $kecamatan,
          $agama,
          $status,
          $pekerjaan,
          $kewarganegaraan,
          $masaBerlaku,
          $foto
        );

        header("Location: index.php");
      } else {
        header("Location: create.php");
      }
    }
  }

  if ($_GET["f"] == "edit") {
    if (isset($_POST['submit'])) {
      foreach ($_POST as $key => $value) {
        if ($key != "nik" && $key != "id")
          ${$key} = encryptSuper(strtoupper($value));
        else
          ${$key} = strtoupper($value);
      }

      $target_dir = "photo/";
      $target_file = $target_dir . $nik . ".png";

      if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) {
        $foto = encryptSuper($target_file);
      } else {
        $foto = "";
      }

      if (isset($masaBerlakuSeumurHidup))
        $masaBerlaku = $masaBerlakuSeumurHidup;
      if ($rt == "")
        $rt = "-";
      if ($rw == "")
        $rw = "-";
      if ($kelDesa == "")
        $kelDesa = "-";
      if ($kecamatan == "")
        $kecamatan = "-";

      $db->update(
        $id,
        $nama,
        $tempatLahir,
        $tanggalLahir,
        $jenisKelamin,
        $golDarah,
        $alamat,
        $rt,
        $rw,
        $kelDesa,
        $kecamatan,
        $agama,
        $status,
        $pekerjaan,
        $kewarganegaraan,
        $masaBerlaku,
        $foto
      );

      header("Location: index.php");
    }
  }

  if ($_GET['f'] == "logout") {
    $sm->logout();
    header("Location: index.php");
  }
}
