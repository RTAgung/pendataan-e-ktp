<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Eh... Katepe</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>

  <head>
    <nav class="navbar navbar-dark bg-primary">
      <div class="container-fluid">
        <a class="navbar-brand" href="index.php">Eh... Katepe</a>
        <div class="d-flex">
          <a class="btn btn-outline-light mx-2" href="login.php">Masuk</a>
          <a class="btn btn-outline-light mx-2" href="signup.php">Daftar</a>
        </div>
      </div>
    </nav>
  </head>

  <main class="container my-5">
    <div class="border border-primary p-5">
      <form action="process.php?f=login" method="POST">
        <legend>Masuk</legend>
        <div class="mb-3">
          <label for="inputUsername" class="form-label">Username</label>
          <input type="text" class="form-control" id="inputUsername" placeholder="username" name="username" required>
        </div>
        <div class="mb-3">
          <label for="inputPassword" class="form-label">Password</label>
          <input type="password" class="form-control" id="inputPassword" placeholder="password" name="password" required>
        </div>
        <div class="mb-3">
          <input type="submit" class="form-control btn btn-primary" value="Masuk" name="submit">
        </div>
      </form>
    </div>
  </main>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>

</html>