<?php
include 'database.php';
include "session_manager.php";

if (!$sm->checkLogin()) header("Location: login.php");

if (isset($_GET['id'])) {
  $id = $_GET['id'];
  $db->delete($id);
  header("Location: index.php");
}
