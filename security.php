<?php
function generateKeyCaesar($text)
{
  return strlen($text);
}

function encryptCaesar($plain)
{
  $cipher = "";
  $key = generateKeyCaesar($plain) % 26;

  for ($i = 0; $i < strlen($plain); $i++) {
    $add = $plain[$i];
    $ascii = ord($plain[$i]);
    if ($ascii > 64 && $ascii < 91) {
      $ascii = $ascii + $key;
      if ($ascii > 90) $ascii -= 26;
      $add = chr($ascii);
    } else if ($ascii > 96 && $ascii < 123) {
      $ascii = $ascii + $key;
      if ($ascii > 122) $ascii -= 26;
      $add = chr($ascii);
    }
    $cipher = $cipher . $add;
  }

  return $cipher;
}

function decryptCaesar($cipher)
{
  $plain = "";
  $key = generateKeyCaesar($cipher) % 26;

  for ($i = 0; $i < strlen($cipher); $i++) {
    $add = $cipher[$i];
    $ascii = ord($cipher[$i]);
    if ($ascii > 64 && $ascii < 91) {
      $ascii = $ascii - $key;
      if ($ascii < 65) $ascii += 26;
      $add = chr($ascii);
    } else if ($ascii > 96 && $ascii < 123) {
      $ascii = $ascii - $key;
      if ($ascii < 97) $ascii += 26;
      $add = chr($ascii);
    }
    $plain = $plain . $add;
  }

  return $plain;
}

function generateKeyScytale($text)
{
  $hasil = sqrt(strlen($text));
  return ceil($hasil);
}

function encryptScytale($plain)
{
  $offset = "`";

  $sizePlain = strlen($plain);
  $currentIndex = 0;

  $key = generateKeyScytale($plain);
  $matrix = array_fill(0, $key, array_fill(0, $key, ""));

  for ($i = 0; $i < $key; $i++) {
    for ($j = 0; $j < $key; $j++) {
      if ($currentIndex < $sizePlain) {
        $char = $plain[$currentIndex];
      } else {
        $char = $offset;
      }
      $matrix[$i][$j] = $char;

      $currentIndex++;
    }
  }

  $cipher = "";

  for ($i = 0; $i < $key; $i++) {
    for ($j = 0; $j < $key; $j++) {
      $cipher = $cipher . $matrix[$j][$i];
    }
  }

  return $cipher;
}

function decryptScytale($cipher)
{
  $offset = "`";

  $sizeCipher = strlen($cipher);
  $currentIndex = 0;

  $key = generateKeyScytale($cipher);
  $matrix = array_fill(0, $key, array_fill(0, $key, ""));

  for ($i = 0; $i < $key; $i++) {
    for ($j = 0; $j < $key; $j++) {
      if ($currentIndex < $sizeCipher) {
        $char = $cipher[$currentIndex];
      } else {
        $char = $offset;
      }
      $matrix[$i][$j] = $char;

      $currentIndex++;
    }
  }

  $plain = "";

  for ($i = 0; $i < $key; $i++) {
    for ($j = 0; $j < $key; $j++) {
      if ($matrix[$j][$i] != $offset) {
        $plain = $plain . $matrix[$j][$i];
      }
    }
  }

  return $plain;
}

function encryptSuper($plain)
{
  $caesar = encryptCaesar($plain);
  $scytale = encryptScytale($caesar);
  return $scytale;
}

function decryptSuper($cipher)
{
  $scytale = decryptScytale($cipher);
  $caesar = decryptCaesar($scytale);
  return $caesar;
}