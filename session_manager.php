<?php
$sm = new SessionManager();

class SessionManager
{
  function __construct()
  {
    session_start();
  }

  function checkLogin()
  {
    if (isset($_SESSION['username'])) {
      return true;
    } else {
      return false;
    }
  }

  function setLogin($username)
  {
    $_SESSION['username'] = $username;
  }

  function logout()
  {
    session_destroy();
  }

  function checkLock()
  {
    if (isset($_SESSION['lock'])) {
      return true;
    } else {
      return false;
    }
  }

  function unlock()
  {
    $_SESSION['lock'] = true;
  }

  function lock()
  {
    unset($_SESSION['lock']);
  }
}
